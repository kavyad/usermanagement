import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AngularFileUploaderModule } from "angular-file-uploader";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,
} from '@angular/material';
import { QuillModule } from 'ngx-quill';
import { NgxSpinnerModule } from 'ngx-spinner';


import { AuthenticationService } from '../providers/authentication.service';
import { UserService } from '../providers/user.service';
import { MembershipService } from '../providers/membership.service';
import { EmailTemplateService } from '../providers/email-template.service';
import { SettingsService } from '../providers/settings.service';
import { ListsService } from '../providers/lists.service';
import { newsLetterService } from '../providers/newsletter.service';
import { StatisticsService } from '../providers/statistics.service';
import { loyaltyService } from '../providers/loyalty.service';
import { loyaltypointService } from '../providers/loyaltypoint.service';
import { RenewalService } from '../providers/renewal.service';

//import { PaymentsGatewayComponent } from './layout/payments-gateway/payments-gateway.component';
//import { PaymentsGatewayModule } from './layout/payments-gateway/payments-gateway.module';
import { AngularHighchartsChartModule } from 'angular-highcharts-chart';



// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AngularFileUploaderModule,
        AppRoutingModule,
        FormsModule,
        NoopAnimationsModule,
        GooglePlaceModule,
        MatAutocompleteModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        QuillModule,
        NgxSpinnerModule,
        //PaymentsGatewayModule,
        AngularHighchartsChartModule
    ],
    declarations: [AppComponent],
    providers: [AuthGuard, 
        AuthenticationService, 
        UserService, 
        MembershipService, 
        EmailTemplateService, 
        SettingsService, 
        ListsService,
        newsLetterService,
        StatisticsService,
        loyaltyService,
        loyaltypointService,
        RenewalService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
