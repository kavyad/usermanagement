import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { NgxSpinnerService } from 'ngx-spinner';


import { AuthenticationService } from '../../providers/authentication.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    constructor(private spinner: NgxSpinnerService, private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public authenticationService: AuthenticationService) {}
    registerform: FormGroup;
    usernameerror = false;
      emailerror = false;
    submitted =  false;
    public userdata: any;
    options = {
      types: ['geocode'],
      componentRestrictions: { country: 'AU' }
    }
    newadress;
    ngOnInit() {
          this.registerform = this.formBuilder.group({
              username: ['', Validators.required],
                    password: ['', Validators.required],
                    firstName: ['', Validators.required],
                    lastName: ['', Validators.required],
                    email: ['', Validators.required],
              phone: ['', Validators.required],
                    dob: ['', Validators.required],
              genderRadios: 'F',
                    address: ['', Validators.required]

          });
      }
      get f() { return this.registerform.controls; }

    onregister() {
      this.spinner.show();
      this.submitted = true;
      if (this.registerform.invalid) {
        this.spinner.hide();
              return;
          }
          this.userdata = {
              username: this.f.username.value,
              password: this.f.password.value,
              firstName: this.f.firstName.value,
              lastName: this.f.lastName.value,
              email: this.f.email.value,
        phone: this.f.phone.value,
              dob: this.f.dob.value,
        genderRadios: this.f.genderRadios.value,
              address: this.newadress,
        role: 3 //users groupid
          }
        this.authenticationService.addregister(this.userdata).then(data => {
                console.log(data);
                if(data['status'] == "success"){
                  alert("Register Done Successfully")
                  this.router.navigate(['/login']);
                }
                else if(data['status'] == "Failed"){
                  alert("Register after some times");
                }
                else if(data['status'] == "Register Exits"){
                    alert("Registration Failed")
                  if(data['value'] == "Username Already Exits."){
                    this.usernameerror = true;
                    this.emailerror = false;
                  }
                  else{
                    this.emailerror = true;
                    this.usernameerror = false;
                  }
                  this.spinner.hide();
                }
                // localStorage.setItem('isLoggedin', 'true');
            //     this.router.navigate(['/signup']);

            },
            error => {
            this.spinner.hide();
            });
    }
    public handleAddressChange(address) {
          // Do some stuff
          this.newadress = address.formatted_address;
      }
}
