import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupComponent } from './signup.component';
import { PageHeaderModule } from './../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxSpinnerModule } from 'ngx-spinner';
import { PaymentsGatewayModule } from './../layout/payments-gateway/payments-gateway.module';


@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule,
    GooglePlaceModule,
    NgxSpinnerModule,
    PaymentsGatewayModule
  ],
  declarations: [SignupComponent]
})
export class SignupModule { }
