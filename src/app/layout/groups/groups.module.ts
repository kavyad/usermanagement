import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupsRoutingModule } from './groups-routing.module';
import { GroupsComponent } from './groups.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule, GroupsRoutingModule, PageHeaderModule, ReactiveFormsModule],
    declarations: [GroupsComponent]
})
export class GroupsModule {}
