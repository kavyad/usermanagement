import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../../providers/user.service';

@Component({
    selector: 'app-groups',
    templateUrl: './groups.component.html',
    styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
	public groups: any;

    constructor(public userservice: UserService) {}

    ngOnInit() {
    	this.userservice.getGroups().then(data => {
	        console.log(data['groups']);
	        this.groups = data['groups'];
	    },
	    error => {
	    });
    }
}
