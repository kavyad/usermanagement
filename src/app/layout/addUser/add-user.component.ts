import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

import {UserService} from '../../../providers/user.service';
import { ListsService } from '../../../providers/lists.service';

@Component({
    selector: 'app-add-user',
    templateUrl: './add-user.component.html',
    styleUrls: ['./add-user.component.scss'],
    animations: [routerTransition()]
})
export class AddUserComponent implements OnInit {

	@ViewChild("placesRef") placesRef : GooglePlaceDirective;
        
	addUserForm: FormGroup;
	public listDetails: any;
	public userDetail: any;
	public showlistadmin: any;
	public listadmin: any;
	usernameerror = false;
	emailerror = false;
	submitted =  false;
	options = {
	    types: ['geocode'],
	    componentRestrictions: { country: 'AU' }
    }
    newadress: any;
    constructor(private listsservice: ListsService, private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public userservice: UserService) {}
    
    ngOnInit() {
    	this.addUserForm = this.formBuilder.group({
            username: ['', Validators.required],
			password: ['', Validators.required],
			firstName: ['', Validators.required],
			lastName: ['', Validators.required],
			email: ['', Validators.required],
			dob: ['', Validators.required],
			phone: ['', Validators.required],
			genderRadios: 'F',
			address: ['', Validators.required],
			sublist: ['', Validators.required]
        });
    	this.listsservice.getlist().then(data => {
	        console.log(data['lists']);
	        this.listDetails = data['lists'];
	    },
	    error => {
	    });
	    if(localStorage.getItem('listadmin') == '1'){
	    	this.showlistadmin = true;
		}
	}
	get f() { return this.addUserForm.controls; }
	addUser(){
		this.submitted = true;
		if (this.addUserForm.invalid) {
            return;
        }
		if(!this.showlistadmin){
			this.listadmin = 3; // users groupid
		}
		this.userDetail = {
			username: this.f.username.value,
			password: this.f.password.value,
			firstName: this.f.firstName.value,
			lastName: this.f.lastName.value,
			email: this.f.email.value,
			dob: this.f.dob.value,
			phone: this.f.phone.value,
			genderRadios: this.f.genderRadios.value,
			address: this.newadress,
			sublist: this.f.sublist.value,
			role: this.listadmin
		}
		this.userservice.addUser(this.userDetail).then(data => {
			if(data['status'] == "success"){
				this.router.navigate(['/user']);
			}
			else if(data['status'] == "Failed"){

			}
			else if(data['status'] == "User Exits"){
				if(data['value'] == "Username Already Exits."){
					this.usernameerror = true;
					this.emailerror = false;
				}
				else{
					this.emailerror = true;
					this.usernameerror = false;
				}
			}
		});
	}
	cancel(){
		this.router.navigate(['/user']);
	}
	public handleAddressChange(address) {
        // Do some stuff
        this.newadress = address.formatted_address;
    }
    change(e){
    	if(e.target.checked){
    		this.listadmin = 4; // list admin groupid
    	}
    	else{
    		this.listadmin = 3; // users groupid
    	}    	
    }
}
