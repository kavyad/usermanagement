import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddUserRoutingModule } from './add-user-routing.module';
import { AddUserComponent } from './add-user.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";


@NgModule({
    imports: [CommonModule, AddUserRoutingModule, PageHeaderModule, ReactiveFormsModule, GooglePlaceModule],
    declarations: [AddUserComponent]
})
export class AddUserModule {}
