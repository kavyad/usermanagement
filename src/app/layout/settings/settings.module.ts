import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@NgModule({
    imports: [CommonModule, FormsModule, SettingsRoutingModule, PageHeaderModule, ReactiveFormsModule, MatSlideToggleModule],
    declarations: [SettingsComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SettingsModule {}
