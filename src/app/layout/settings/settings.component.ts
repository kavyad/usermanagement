import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType} from '@angular/common/http';

import { SettingsService } from '../../../providers/settings.service'


@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
	public currentDate: any;
	public settingDetail = {
		'webname': '',
		'sitedir': '',
		'companylogo': '',
		'webemail': '',
		'payemail': '',
		'points': '',
		//'datetype': '',
		'allowregistration': '',
		'regverification': '',
		'registrationnotification': '',
		'defaultcurrency': '',
		'webphone': '',
		'listadmin': ''
	};
	public update = false;
	percentDone: number;
  	uploadSuccess: boolean;

    constructor(private http: HttpClient, private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public settingsservice: SettingsService) {}

    ngOnInit() {
    	/*this.settingsForm = this.formBuilder.group({
            webname: ['', Validators.required],
			sitedir: ['', Validators.required],
			companylogo: ['', Validators.required],
			webemail: ['', Validators.required],
			payemail: ['', Validators.required],
			invoicelogo: ['', Validators.required],
			datetype: ['Select Date Format'],
			allowregistration: ['', Validators.required],
			regverification: ['', Validators.required],
			registrationnotification: ['', Validators.required],
			defaultcurrency: ['', Validators.required]
        });*/
    	this.currentDate = Date.now();
    	this.settingsservice.getSettings().then(data => {
			if(data['status']){
				this.settingDetail.webname = data['settings'][0].webname;
				this.settingDetail.sitedir = data['settings'][0].sitedir;
				this.settingDetail.companylogo = data['settings'][0].companylogo;
				this.settingDetail.webemail = data['settings'][0].webemail;
				this.settingDetail.payemail = data['settings'][0].payemail;
				this.settingDetail.points = data['settings'][0].points;
				//this.settingDetail.datetype = data['settings'][0].datetype;
				this.settingDetail.allowregistration = data['settings'][0].allowregistration;
				this.settingDetail.regverification = data['settings'][0].regverification;
				this.settingDetail.registrationnotification = data['settings'][0].registrationnotification;
				this.settingDetail.defaultcurrency = data['settings'][0].defaultcurrency;
				this.settingDetail.webphone = data['settings'][0].webphone;
				this.settingDetail.listadmin = data['settings'][0].listadmin;
			}
		});

		if(this.settingDetail.webname !== '' || this.settingDetail.webname !== undefined){
			this.update = true;
		}
    }

    saveSettings(){    	
		this.settingsservice.saveSettings(this.settingDetail).then(data => {
			if(data['status']){
				window.location.reload();
			}
		});
    }

    updateSettings(){
		this.settingsservice.updateSettings(this.settingDetail).then(data => {
			if(data['status']){
				window.location.reload();
			}
		});
    }

    uploadCompanyLogo(files: File[]){
	    //pick from one of the 4 styles of file uploads below
	    this.uploadAndProgress(files);
	    this.settingDetail.companylogo = files[0].name;
	}

	// uploadInvoiceLogo(files: File[]){
	//     //pick from one of the 4 styles of file uploads below
	//     this.uploadAndProgress(files);
	//     this.settingDetail.invoicelogo = files[0].name;
	// }

    uploadAndProgress(files: File[]){
	    console.log(files)
	    var formData = new FormData();
	    Array.from(files).forEach(f => formData.append('file',f))
	    
	    this.http.post('https:/click365.com.au/usermanagement/uploadLogo.php', formData, {reportProgress: true, observe: 'events'})
	      .subscribe(event => {
	        if (event.type === HttpEventType.UploadProgress) {
	          this.percentDone = Math.round(100 * event.loaded / event.total);
	        } else if (event instanceof HttpResponse) {
	          this.uploadSuccess = true;
	        }
	    });
	  }
}
