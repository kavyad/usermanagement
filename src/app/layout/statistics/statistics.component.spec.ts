import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { StatisticsModule } from './statistics.module'
import { StatisticsComponent } from './statistics.component';

describe('StatisticsComponent', () => {
    let component: StatisticsComponent;
    let fixture: ComponentFixture<StatisticsComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                imports: [
                  StatisticsModule,
                  RouterTestingModule,
                  BrowserAnimationsModule,
                ],
                declarations: [StatisticsComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(StatisticsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
