import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { StatisticsRoutingModule } from './statistics-routing.module';
import { StatisticsComponent } from './statistics.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
    imports: [CommonModule, 
    Ng2Charts, 
    StatisticsRoutingModule, 
    PageHeaderModule, 
    ReactiveFormsModule, 
    MatTabsModule, 
    MatProgressBarModule,
    MatGridListModule],
    declarations: [StatisticsComponent]
})
export class StatisticsModule {}
