import { Component, OnInit } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';

import { StatisticsService } from '../../../providers/statistics.service';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {

    newsletterId;
    stats = [];
    line = [];
    pie = [];
    newsletterDetail;
    newsletterDate;
    subject;
    list;
    totalSent;
    notSent;
    totalSentPerc;
    notSentPerc;
    sent;
    domains;
    links;
    openCount;
    clickCount;    
    chartData = [
        {data:0,label:'Opened'},
        {data:0,label:'Clicked'}
    ];
    chartOptions = {
        responsive: true
    };
    chartData1;



    //chartLabels = ['January', 'February', 'Mars', 'April'];
    chartLabels=['0hr', '12hr', '24hr', '36hr', '48hr', '60hr', '72hr'];

    constructor(public statisticsservice: StatisticsService, private route: ActivatedRoute, public router: Router) {
        this.route.queryParams.subscribe(params => {this.newsletterId = params['id'];});
        this.statisticsservice.gettimeline(this.newsletterId).then(data => {
            if(data['status']){
                //console.log(data['reports']);
                let opencount = [];
                let clickcount = [];
                let graphdata = [];
                let graphlabels = [];
                for(let i = 0; i < data['reports']['open'].length; i++){
                    opencount.push(data['reports']['open'][i]['opencount']);
                    graphlabels.push(data['reports']['open'][i]['hr']);
                }
                opencount.push(1);
                for(let i = 0; i < data['reports']['click'].length; i++){
                    clickcount.push(data['reports']['click'][i]['clickcount']);
                    graphlabels.push(data['reports']['open'][i]['hr']);
                }
                clickcount.push(6);
                graphdata.push({data:opencount, label:'Opened'},{data:clickcount, label:'Clicked'});
                //console.log(graphdata);
                this.chartData = graphdata;
                console.log(this.chartData);
                console.log(typeof(this.chartData));
                this.chartLabels = graphlabels;
            }                
        },
        error => {
        });
    }


    ngOnInit() {
        //this.route.queryParams.subscribe(params => {this.newsletterId = params['id'];});
        // this.statisticsservice.gettimeline(this.newsletterId).then(data => {
        //     if(data['status']){
        //         //console.log(data['reports']);
        //         let opencount = [];
        //         let clickcount = [];
        //         let graphdata = [];
        //         let graphlabels = [];
        //         for(let i = 0; i < data['reports']['open'].length; i++){
        //             opencount.push(data['reports']['open'][i]['opencount']);
        //             graphlabels.push(data['reports']['open'][i]['hr']);
        //         }
        //         for(let i = 0; i < data['reports']['click'].length; i++){
        //             clickcount.push(data['reports']['click'][i]['clickcount']);
        //             graphlabels.push(data['reports']['open'][i]['hr']);
        //         }
        //         graphdata.push({data:opencount, label:'Opened'},{data:clickcount, label:'Clicked'});
        //         //console.log(graphdata);
        //         this.chartData = graphdata;
        //         this.chartLabels = graphlabels;
        //         console.log(this.chartData);
        //         this.chartData1 = [
        //             { data: [330, 600, 260], label: 'Account A' },
        //             { data: [120, 455, 100, 340], label: 'Account B' },
        //             { data: [45, 67, 800, 500], label: 'Account C' }
        //             ];
        //         }
                
        // },
        // error => {
        // });
        this.statisticsservice.getReportById(this.newsletterId).then(data => {
            if(data['status']){
                document.getElementById('messageTemplate').innerHTML = JSON.parse(data['reports'][0]['message']);
                this.newsletterDetail = JSON.parse(data['reports'][0]['message']);
                this.newsletterDate = data['reports'][0]['date'];
                this.subject = data['reports'][0]['subject'];
                this.list = data['reports'][0]['list'];
                this.totalSent = data['reports'][0]['totalSent'];
                this.notSent = data['reports'][0]['notSent'];
                this.sent = data['reports'][0]['totalSent'] - data['reports'][0]['notSent'];
                this.totalSentPerc = (((data['reports'][0]['totalSent'] - data['reports'][0]['notSent']) / data['reports'][0]['totalSent']) * 100);
                this.notSentPerc = ((data['reports'][0]['notSent'] / data['reports'][0]['totalSent']) * 100);
                this.clickCount = data['reports'][0]['clickCount'];
            }
        },
        error => {

        });
      this.statisticsservice.getEmailDomain(this.newsletterId).then(data => {
          if(data['status']){
                this.domains = data['reports'];
            }
      },
      error => {
      });
        this.statisticsservice.getclickCounts(this.newsletterId).then(data => {
            if(data['status']){
                this.links = data['reports'];
                this.openCount = data['reports'][0]['openCount'];
            }
        },
        error => {
        });
    }
}
