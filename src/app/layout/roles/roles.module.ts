import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule, RolesRoutingModule, PageHeaderModule, ReactiveFormsModule],
    declarations: [RolesComponent]
})
export class RolesModule {}
