import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

import { UserService } from '../../../providers/user.service';
import { ListsService } from '../../../providers/lists.service';
import { StatisticsService } from '../../../providers/statistics.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as Highcharts from 'highcharts';


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    userCount;
    listCount;
    newsletterDetail;
    newsletterId;
    chartData: number[] = [2,4];
    chartOptions = {
        responsive: true
    };
    chartLabels=['Opened', 'Clicked'];

    Highcharts = Highcharts; // required
    chartConstructor = 'chart'; // optional string, defaults to 'chart'
    chartOptions1 = {}; // required
    //chartCallback = function (chart) { ... } // optional function, defaults to null
    updateFlag = false; // optional boolean

    constructor(private spinner: NgxSpinnerService, public userservice: UserService, public listsservice: ListsService, public statisticsservice: StatisticsService) {
        this.chartOptions1 = {
            chart: {
              type: 'pie'
            },
            title: {
              text: 'User Count',
              style: {
                  color : "#ff0000"
              }
            },
            xAxis: {
              categories: ['Jan', 'Feb', 'Mar']
            },
            yAxis: {
              title: {
                  text: 'Count'
              }
            },
            series: [{
              name: 'user counts',
              data: [['a', 1],
                            ['b', 0],
                            ['c', 4]]
            }]
        }
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.jpg',
                label: 'First slide label',
                text:
                    'Nulla vitae elit libero, a pharetra augue mollis interdum.'
            },
            {
                imagePath: 'assets/images/slider2.jpg',
                label: 'Second slide label',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                imagePath: 'assets/images/slider3.jpg',
                label: 'Third slide label',
                text:
                    'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
            }
        );
     
        this.statisticsservice.getLatestReport().then(data => {
            //console.log(data['reports']);
            this.newsletterDetail = data['reports'][0];
            //console.log(this.newsletterDetail);
            document.getElementById('newsletter').innerHTML = JSON.parse(data['reports'][0]['message']);
            this.newsletterId = data['reports'][0]['id'];
            this.statisticsservice.getopenclickcount(this.newsletterId).then(data => {
                if(data['status'] == 'success'){
                    this.chartData = data['reports'];
                    console.log(data['reports']);
                    this.chartOptions1 = {
                        chart: {
                          type: 'pie'
                        },
                        title: {
                          text: 'User Count',
                          style: {
                              color : "#ff0000"
                          }
                        },
                        xAxis: {
                          categories: ['Jan', 'Feb', 'Mar']
                        },
                        yAxis: {
                          title: {
                              text: 'Count'
                          }
                        },
                        series: [{
                          name: 'user counts',
                          data: [
                              ['Opened', data['reports'][0]],
                              ['Clicked', data['reports'][1]]
                          ]
                        }]
                    }
                }                
            },
            error => {
            });

        },
        error => {
        });
        this.alerts.push(
            {
                id: 1,
                type: 'success',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            },
            {
                id: 2,
                type: 'warning',
                message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            }
        );
    }

    ngOnInit() {   
        this.userservice.getUsers().then(data => {
            console.log(data['user'].length);
            this.userCount = data['user'].length;
        },
        error => {
        });
        this.listsservice.getlist().then(data => {
            console.log(data['lists'].length);
            this.listCount = data['lists'].length;
        },
        error => {
        });
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
