import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

import {UserService} from '../../../providers/user.service';

@Component({
    selector: 'app-edit-user',
    templateUrl: './edit-user.component.html',
    styleUrls: ['./edit-user.component.scss'],
    animations: [routerTransition()]
})
export class EditUserComponent implements OnInit {	

	@ViewChild("placesRef") placesRef : GooglePlaceDirective;

	userDetail = {
		'id': '',
		'username': '',
		'firstName': '',
		'email': '',
		'dob': '',
		'password': '',
		'lastName': '',
		'genderRadios': '',
		'address': ''
	};
	options = {
	    types: ['geocode'],
	    componentRestrictions: { country: 'AU' }
    }
    
    constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public userservice: UserService) {}

    ngOnInit() {
    	this.route.queryParams.subscribe(params => {this.userDetail.id = params['id'];})
    	this.userservice.getUser(this.userDetail.id).then(data => {
			if(data['status']){
				this.userDetail.id = data['user'][0].id;
				this.userDetail.username = data['user'][0].username;
				this.userDetail.firstName = data['user'][0].firstName;
				this.userDetail.email = data['user'][0].email;
				this.userDetail.dob = data['user'][0].dob;
				this.userDetail.password = data['user'][0].password;
				this.userDetail.lastName = data['user'][0].lastName;
				this.userDetail.address = data['user'][0].address;
				this.userDetail.genderRadios = data['user'][0].gender;
			}
		});
	}
	editUser(){
		this.userservice.editUser(this.userDetail).then(data => {
			if(data['status']){
				this.router.navigate(['/user']);
			}
		});
	}
	cancel(){
		this.userDetail = {
			'id': '',
			'username': '',
			'firstName': '',
			'email': '',
			'dob': '',
			'password': '',
			'lastName': '',
			'genderRadios': '',
			'address': ''
		};
		this.router.navigate(['/user']);
	}
	public handleAddressChange(address) {
        // Do some stuff
        this.userDetail.address = address.formatted_address;
    }
}
