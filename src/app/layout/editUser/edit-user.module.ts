import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EditUserRoutingModule } from './edit-user-routing.module';
import { EditUserComponent } from './edit-user.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
    imports: [CommonModule, EditUserRoutingModule, PageHeaderModule, ReactiveFormsModule, FormsModule, GooglePlaceModule],
    declarations: [EditUserComponent]
})
export class EditUserModule {}
