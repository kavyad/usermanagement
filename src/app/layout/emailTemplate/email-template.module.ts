import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EmailTemplateRoutingModule } from './email-template-routing.module';
import { EmailTemplateComponent } from './email-template.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';

import { MatSortModule } from '@angular/material/sort';


@NgModule({
    imports: [MatTableModule,MatPaginatorModule,MatInputModule,MatSortModule,FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(), CommonModule, FormsModule, EmailTemplateRoutingModule, PageHeaderModule, ReactiveFormsModule],
    declarations: [EmailTemplateComponent]
})
export class EmailTemplateModule {}
