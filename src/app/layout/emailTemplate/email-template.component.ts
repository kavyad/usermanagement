import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { EmailTemplateService } from '../../../providers/email-template.service';


@Component({
    selector: 'app-email-template',
    templateUrl: './email-template.component.html',
    styleUrls: ['./email-template.component.scss'],
    animations: [routerTransition()]
})
export class EmailTemplateComponent implements OnInit {
	emailTemplateForm: FormGroup;
	public emilTemDetails: any;
	editEmailTemDetails = {
		'id': '',
		'name': '',
		'subject': '',
		'body': ''
	};
	public emailTemplates: any;
	public showTable = true;
	public showEdit = false;
	public new = true;
	defaultTemplate;
	public options: Object = {
	    charCounterCount: true,
	    // Set the image upload parameter.
	    imageUploadParam: 'image_param',

	    // Set the image upload URL.
	    imageUploadURL: 'https://click365.com.au/usermanagement/images',

	    // Additional upload params.
	    imageUploadParams: {id: 'my_editor'},

	    // Set request type.
	    imageUploadMethod: 'POST',

	    // Set max image size to 5MB.
	    imageMaxSize: 5 * 1024 * 1024,

	    // Allow to upload PNG and JPG.
	    imageAllowedTypes: ['jpeg', 'jpg', 'png'],
    	events:  {
			'froalaEditor.initialized':  function () {
				console.log('initialized');
			},
  			'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
			    //Your code 
			    if  (images.length) {
					// Create a File Reader.
					const  reader  =  new  FileReader();
					// Set the reader to insert images when they are loaded.
					reader.onload  =  (ev)  =>  {
					const  result  =  ev.target['result'];
					editor.image.insert(result,  null,  null,  editor.image.get());
					console.log(ev,  editor.image,  ev.target['result'])
					};
					// Read image as base64.
					reader.readAsDataURL(images[0]);
		    	}
		    	// Stop default upload chain.
		    	return  false;
		  	}
		}
	};

	displayedColumns = ['templatename', 'subject','action'];
	dataSource: MatTableDataSource<emailtemplateData>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

    constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public emailtemplateservice: EmailTemplateService) {
    	this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
    }

    ngOnInit() {
    	this.emailTemplateForm = this.formBuilder.group({
            temname: ['', Validators.required],
			temsub: ['', Validators.required],
			tembody: this.defaultTemplate
        });
        this.emailtemplateservice.getEmailTemp().then(data => {
	        console.log(data['emailTemplate']);
	        if(data['status'] == 'success'){
        		debugger;
	            const emailtemplate: emailtemplateData[] = [];
	            for (let i = 0; i < data['emailTemplate'].length; i++) {emailtemplate.push(data['emailTemplate'][i]); }
	            this.dataSource = new MatTableDataSource(emailtemplate);
	            this.dataSource.paginator = this.paginator;
	            this.dataSource.sort = this.sort;
	        	this.emailTemplates = data['emailTemplate'];
	          }
	    },
	    error => {
	    });
	}
	get f() { return this.emailTemplateForm.controls; }

	addet(){
		this.showTable = false;
		this.new = true;
	}

	cancel(){
		window.location.reload();
	}

	editet(id){
        this.emailtemplateservice.getEmailTempById(id).then(data => {
	        console.log(data['emailTemplate']);
	        this.editEmailTemDetails.id = data['emailTemplate'][0].id;
	        this.editEmailTemDetails.name = data['emailTemplate'][0].name;
	        this.editEmailTemDetails.subject = data['emailTemplate'][0].subject;
	        this.editEmailTemDetails.body = data['emailTemplate'][0].body;
	        			
			this.new = false;
			this.showEdit = true;
	    },
	    error => {
	    });
	}

	deleteet(id){
        this.emailtemplateservice.deleteEmailTempById(id).then(data => {
	        console.log(data['emailTemplate']);
	        window.location.reload();
	    },
	    error => {
	    });
	}

	addEmailTemplate(){
		this.emilTemDetails = {
			name: this.f.temname.value,
			subject: this.f.temsub.value,
			body: this.f.tembody.value
		}
		this.emailtemplateservice.addEmailTemp(this.emilTemDetails).then(data => {
			if(data['status']){
				window.location.reload();
				this.router.navigate(['/email-template']);
			}
		});
	}

	updateEmailTemplate(){
		this.emailtemplateservice.updateEmailTemp(this.editEmailTemDetails).then(data => {
			if(data['status']){
				this.showTable = true;
				this.showEdit = false;
				this.new = true;
				window.location.reload();
			}
		});
	}

	// setFocus(quill) {
	// 	this.imageResizeInstance = quill.addModule('imageResize');
	// 	quill.focus();
	// }
}
export interface emailtemplateData {
  templatename: string;
  subject: string;
  action: string;
}