import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { loyaltyService } from '../../../providers/loyalty.service';

@Component({
    selector: 'app-loyalty',
    templateUrl: './loyalty.component.html',
    styleUrls: ['./loyalty.component.scss'],
      animations: [routerTransition()]
})
export class loyaltyComponent implements OnInit {
    constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public LoyaltyService: loyaltyService) {}
    loyaltyform: FormGroup;
    public loyaltydata: any;
      submitted =  false;

    ngOnInit() {
        this.loyaltyform = this.formBuilder.group({
              Name: ['', Validators.required],
              Points: ['', Validators.required]
        });
    }

    get f(){ return this.loyaltyform.controls; }
    addloyalty(){
    this.loyaltydata = false;
    this.loyaltydata = true;
  }


    cancel(){
  		this.router.navigate(['/loyalty-display']);
  	}

    onloyalty() {
      this.submitted = true;
      if (this.loyaltyform.invalid) {
              return;
          }
      this.loyaltydata = {
        name: this.f.Name.value,
        points: this.f.Points.value
      }
      this.LoyaltyService.addloyalty(this.loyaltydata).then(data => {
        if(data['status'] == "success"){
  				this.router.navigate(['/loyalty-display']);
  			}
  			else if(data['status'] == "Failed"){


  			}
          console.log(data);
        },
        error => {
        });
      }
}
