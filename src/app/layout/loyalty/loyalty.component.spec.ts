import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { loyaltyComponent } from './loyalty.component';

describe('loyaltyComponent', () => {
    let component: loyaltyComponent;
    let fixture: ComponentFixture<loyaltyComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [loyaltyComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(loyaltyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
