import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { loyaltyRoutingModule } from './loyalty-routing.module';
import { loyaltyComponent } from './loyalty.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule, loyaltyRoutingModule, PageHeaderModule, ReactiveFormsModule],
    declarations: [loyaltyComponent]
})
export class loyaltyModule {}
