import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { loyaltyComponent } from './loyalty.component';

const routes: Routes = [
    {
        path: '',
        component: loyaltyComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class loyaltyRoutingModule {}
