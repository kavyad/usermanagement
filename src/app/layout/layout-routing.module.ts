import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'user', loadChildren: './userDetail/user-detail.module#UserDetailModule' },
            { path: 'adduser', loadChildren: './addUser/add-user.module#AddUserModule' },
            { path: 'membership', loadChildren: './membership/membership.module#MembershipModule' },
            { path: 'newsletter', loadChildren: './newsletter/newsletter.module#NewsletterModule' },
            { path: 'email-template', loadChildren: './emailTemplate/email-template.module#EmailTemplateModule' },
            { path: 'edit-user', loadChildren: './editUser/edit-user.module#EditUserModule' },
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule'},
            { path: 'lists', loadChildren: './lists/lists.module#ListsModule'},
            { path: 'subscribers', loadChildren: './subscriber/subscriber.module#SubscriberModule'},
            { path: 'roles', loadChildren: './roles/roles.module#RolesModule'},
            { path: 'groups', loadChildren: './groups/groups.module#GroupsModule'},
            { path: 'statistics', loadChildren: './statistics/statistics.module#StatisticsModule'},            
            { path: 'reports', loadChildren: './reports/reports.module#ReportsModule'},
            { path: 'loyalty', loadChildren: './loyalty/loyalty.module#loyaltyModule' },
            { path: 'loyaltypoint', loadChildren: './loyaltypoint/loyaltypoint.module#loyaltypointModule'},
            { path: 'loyalty-display', loadChildren: './loyaltyDisplay/loyalty-display.module#LoyaltyDisplayModule' },
            { path: 'profile', loadChildren: './profile/profile.module#ProfileModule'},
            { path: 'transactions', loadChildren: './transactions/transactions.module#TransactionsModule'},
            { path: 'catalogue', loadChildren: './catalogue/catalogue.module#CatalogueModule'},
            { path: 'renewal', loadChildren: './renewal/renewal.module#RenewalModule'}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
