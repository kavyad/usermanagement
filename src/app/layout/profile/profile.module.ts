import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';



@NgModule({
    imports: [CommonModule, ProfileRoutingModule, PageHeaderModule,ReactiveFormsModule],
    declarations: [ProfileComponent]
})
export class ProfileModule {}