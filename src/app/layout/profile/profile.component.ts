import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  users;
    constructor() {}

    ngOnInit() {
      this.users = JSON.parse(localStorage.getItem('currentUser'));
      console.log(this.users);
    }
}
