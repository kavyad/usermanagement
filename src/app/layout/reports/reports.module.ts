import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { ReportsComponent } from './reports.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field'
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';

@NgModule({
    imports: [CommonModule,
    ReportsRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    PageHeaderModule,
    ReactiveFormsModule,
    MatSortModule],
    declarations: [ReportsComponent]
})
export class ReportsModule {}
