import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { StatisticsService } from '../../../providers/statistics.service';

@Component({
    selector: 'app-blank-page',
    templateUrl: './reports.component.html',
    styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
	displayedColumns = ['status', 'name', 'sent', 'openRate', 'bounceRate', 'linkRate', 'date', 'stats'];
	dataSource: MatTableDataSource<ReportsData>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	reportCount: any;

    constructor(public statisticsservice: StatisticsService, public router: Router) {}

    ngOnInit() {
    	this.statisticsservice.getreports().then(data => {
	        console.log(data['reports']);
	        if(data['status']){
		        const reports: ReportsData[] = [];
		        this.reportCount = data['reports'].length;
		        for (let i = 0; i < data['reports'].length; i++) { reports.push(data['reports'][i]); }
		        this.dataSource = new MatTableDataSource(reports);
				this.dataSource.paginator = this.paginator;
				this.dataSource.sort = this.sort;
		        //this.users = data['user'];
		    }
	    },
	    error => {
	    });
    }
    redirect(id){
		this.router.navigate(['/statistics'],{queryParams:{id:id}});
	}
}
export interface ReportsData {
  	status : string,
	name : string,
	sent : string,
	openRate : string,
	bounceRate : string,
	linkRate : string,
	date : string,
	stats : string
}