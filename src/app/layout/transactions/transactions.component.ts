import { Component, OnInit } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ViewChild } from '@angular/core';

@Component({
    selector: 'app-transactions',
    templateUrl: './transactions.component.html',
    styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  users;
  displayedColumns = ['transactionid', 'userid','membershipid','amount','renewaldate','expirydate'];
  dataSource: MatTableDataSource<transactionsData>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor() {}

  ngOnInit() {
    this.users = JSON.parse(localStorage.getItem('currentUser'));
    console.log(this.users);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;    
  }
}
export interface transactionsData {
  transactionsid: string;
  userid: boolean;
  membershipid: string;
  amount:string;
  renewaldate:string;
  expirydate:string;
}
