import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { PageHeaderModule } from './../../shared';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field'


@NgModule({
    imports: [CommonModule,
      TransactionsRoutingModule,
      PageHeaderModule,
      MatTableModule,
      MatPaginatorModule,
      MatFormFieldModule,
      MatInputModule ],
    declarations: [TransactionsComponent]
})
export class TransactionsModule {}
