import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../../../providers/user.service';
import { ListsService } from '../../../providers/lists.service';

@Component({
    selector: 'app-subscriber',
    templateUrl: './subscriber.component.html',
    styleUrls: ['./subscriber.component.scss']
})
export class SubscriberComponent implements OnInit {
    public users: any;
    isListAdmin = false;
    listDetails;

    constructor(private listsservice: ListsService, public userservice: UserService) {
        if(localStorage.getItem('isListAdmin') == 'true'){
            this.isListAdmin = true;
        }
    }

    ngOnInit() {
        this.listsservice.getlist().then(data => {
            console.log(data['lists']);
            this.listDetails = data['lists'];
        },
        error => {
        });
        if(this.isListAdmin){
            let listId = JSON.parse(localStorage.getItem('currentUser'));
            this.userservice.getUsersSubscriptionList(listId['admin_listid']).then(data => {
                console.log(data['users']);
                this.users = data['users'];
            });
        }
        else{
            this.userservice.getUsersSubscribtion().then(data => {
                console.log(data['users']);
                this.users = data['users'];
            },
            error => {
            });
        }
    }

    selectUser(value){
        if(value != ''){
            this.userservice.getUsersSubscriptionList(value).then(data => {
                console.log(data['users']);
                this.users = data['users'];
            });
        }
        else{
            this.userservice.getUsersSubscribtion().then(data => {
                console.log(data['users']);
                this.users = data['users'];
            },
            error => {
            });
        }
    }
}
