import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SubscriberRoutingModule } from './subscriber-routing.module';
import { SubscriberComponent } from './subscriber.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule, SubscriberRoutingModule, PageHeaderModule, ReactiveFormsModule],
    declarations: [SubscriberComponent]
})
export class SubscriberModule {}
