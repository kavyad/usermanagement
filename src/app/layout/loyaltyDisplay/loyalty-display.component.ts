import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { loyaltyService } from '../../../providers/loyalty.service';

@Component({
    selector: 'app-loyaltyDisplay',
    templateUrl: './loyalty-display.component.html',
    styleUrls: ['./loyalty-display.component.scss']
})
export class LoyaltyDisplayComponent implements OnInit {
	displayedColumns = ['name', 'points'];
	dataSource: MatTableDataSource<loyaltydisplay>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	public loyaltydisplay: any;
    constructor(public loyaltyservice: loyaltyService, public router: Router) {}

    ngOnInit() {
		this.loyaltyservice.getLoyalty().then(data => {
	        console.log(data['loyalty']);
	        const loyalty: loyaltydisplay[] = [];
	        for (let i = 0; i < data['loyalty'].length; i++) { loyalty.push(data['loyalty'][i]); }
	        this.dataSource = new MatTableDataSource(loyalty);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
	        //this.users = data['user'];
	    },
	    error => {
	    });
	}

	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
		this.dataSource.filter = filterValue;
	}

	editloyalty(id){
		this.router.navigate(['/edit-loyalty'],{queryParams:{id:id}});
	}

	deleteloyalty(id){
		this.loyaltyservice.deleteloyalty(id).then(data => {
	        window.location.reload();
	    },
	    error => {
	    });
	}
}

export interface loyaltydisplay {
  name: string;
  points: boolean;
}
