import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoyaltyDisplayRoutingModule } from './loyalty-display-routing.module';
import { LoyaltyDisplayComponent } from './loyalty-display.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field'
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule,
      LoyaltyDisplayRoutingModule,
      MatTableModule,
      MatPaginatorModule,
      MatFormFieldModule,
      MatInputModule,
      ReactiveFormsModule],
    declarations: [LoyaltyDisplayComponent]
})
export class LoyaltyDisplayModule {}
 
