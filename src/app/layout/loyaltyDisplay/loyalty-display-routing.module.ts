import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoyaltyDisplayComponent } from './loyalty-display.component';

const routes: Routes = [
    {
        path: '',
        component: LoyaltyDisplayComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoyaltyDisplayRoutingModule {}
