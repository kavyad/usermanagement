import { LoyaltyDisplayModule } from './loyalty-display.module';

describe('LoyaltyDisplayModule', () => {
    let loyaltyDisplayModule: LoyaltyDisplayModule;

    beforeEach(() => {
        loyaltyDisplayModule = new LoyaltyDisplayModule();
    });

    it('should create an instance', () => {
        expect(loyaltyDisplayModule).toBeTruthy();
    });
});
