import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-payments-gateway',
    templateUrl: './payments-gateway.component.html',
    styleUrls: ['./payments-gateway.component.scss']
})
export class PaymentsGatewayComponent implements OnInit {

	creditCardForm: FormGroup;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.creditCardForm = this.formBuilder.group({
            name: [ '' ],
            number: [ '' ],
            cvc: [ '' ],
            expiry: [ '' ],
        });
    }
    get f() { return this.creditCardForm.controls; }
    submit(value) {
        if (this.creditCardForm.invalid) {
            return;
        }
        let cardDetail = {
			name: this.f.name.value,
			number: this.f.number.value,
			cvc: this.f.cvc.value,
			expiry: this.f.expiry.value
		}
        console.log(cardDetail);
        // Do something with the result
    }
}
