import { PaymentsGatewayModule } from './payments-gateway.module';

describe('PaymentsGatewayModule', () => {
    let paymentsGatewayModule: PaymentsGatewayModule;

    beforeEach(() => {
        paymentsGatewayModule = new PaymentsGatewayModule();
    });

    it('should create an instance', () => {
        expect(paymentsGatewayModule).toBeTruthy();
    });
});
