import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaymentsGatewayRoutingModule } from './payments-gateway-routing.module';
import { PaymentsGatewayComponent } from './payments-gateway.component';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule, PaymentsGatewayRoutingModule, ReactiveFormsModule],
    declarations: [PaymentsGatewayComponent]
})
export class PaymentsGatewayModule {}
