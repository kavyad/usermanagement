import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentsGatewayComponent } from './payments-gateway.component';

const routes: Routes = [
    {
        path: '',
        component: PaymentsGatewayComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PaymentsGatewayRoutingModule {}
