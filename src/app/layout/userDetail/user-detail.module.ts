import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserDetailRoutingModule } from './user-detail-routing.module';
import { UserDetailComponent } from './user-detail.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field'
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { MatSortModule } from '@angular/material/sort';
@NgModule({
    imports: [
        CommonModule,
        UserDetailRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        PageHeaderModule,
        ReactiveFormsModule,
        MatSortModule
    ],
    declarations: [UserDetailComponent]
})
export class UserDetailModule {}
