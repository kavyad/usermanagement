import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import * as XLSX from 'xlsx';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import 'jspdf-autotable';

import {UserService} from '../../../providers/user.service';

@Component({
    selector: 'app-user-detail',
    templateUrl: './user-detail.component.html',
    styleUrls: ['./user-detail.component.scss'],
    providers: [{ provide: 'Window',  useValue: window }]
})
export class UserDetailComponent implements OnInit {
	displayedColumns = ['firstname', 'lastname', 'email', 'role', 'actions'];
	dataSource: MatTableDataSource<UserData>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	exportData = [];

	//public users: any;
    constructor(public userservice: UserService, public router: Router) {}

    ngOnInit() {
		this.userservice.getUsers().then(data => {
	        console.log(data['user']);
	        const users: UserData[] = [];
	        for (let i = 0; i < data['user'].length; i++) { users.push(data['user'][i]); this.exportData.push(data['user'][i]); }
	        this.dataSource = new MatTableDataSource(users);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
	        //this.users = data['user'];
	    },
	    error => {
	    });
	}

	applyFilter(filterValue: string) {
		filterValue = filterValue.trim(); // Remove whitespace
		filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
		this.dataSource.filter = filterValue;
	}

	edituser(id){
		this.router.navigate(['/edit-user'],{queryParams:{id:id}});
	}

	deleteuser(id){
		this.userservice.deleteUser(id).then(data => {
	        window.location.reload();
	    },
	    error => {
	    });
	}

	ExportToPdf(){
	  	//var data = document.getElementById('contentToConvert');  
		// html2canvas(table).then(canvas => {  
		// 	// Few necessary setting options  
		// 	var imgWidth = 208;   
		// 	var pageHeight = 295;    
		// 	var imgHeight = canvas.height * imgWidth / canvas.width;  
		// 	var heightLeft = imgHeight;  

		// 	const contentDataURL = canvas.toDataURL('image/png')  
		// 	let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
		// 	var position = 0;  
		// 	pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);			
		// 	pdf.save('users_'+Date()+'.pdf'); // Generated PDF   
		// });
		var doc = new jspdf();
		var col = ["First Name","Last Name", "Email", "Role"];
		var rows = [];
		this.exportData.forEach(element => {      
	        var temp = [element.FirstName, element.LastName, element.email, element.role];
	        rows.push(temp);

	    }); 
		doc.autoTable(col, rows, { startY: 10 });		
		doc.save('users_'+Date()+'.pdf');
	}

	ExportTOExcel(){
		// const ws: XLSX.WorkSheet=XLSX.utils.table_to_sheet(this.table.nativeElement);
		// const wb: XLSX.WorkBook = XLSX.utils.book_new();
		// XLSX.utils.book_append_sheet(wb, ws, 'User Details');

		// /* save to file */
		// XLSX.writeFile(wb, 'users_'+Date()+'.xlsx');
	  	const workBook = XLSX.utils.book_new(); // create a new blank book
	    const workSheet = XLSX.utils.json_to_sheet(this.exportData);

	    XLSX.utils.book_append_sheet(workBook, workSheet, 'data'); // add the worksheet to the book
	    XLSX.writeFile(workBook, 'users_'+Date()+'.xlsx');

	    XLSX.utils.sheet_to_csv(workSheet)
	    XLSX.writeFile(workBook, 'users_'+Date()+'.xlsx');
	}

	ExportTOCsv(){
	  	const workBook = XLSX.utils.book_new(); // create a new blank book
	    const workSheet = XLSX.utils.json_to_sheet(this.exportData);
	    const csv = XLSX.utils.sheet_to_csv(workSheet);

	    XLSX.utils.book_append_sheet(workBook, workSheet, 'data'); // add the worksheet to the book
	    XLSX.writeFile(workBook, 'users_'+Date()+'.csv');
	}
}

export interface UserData {
  firstname: string;
  lastname: string;
  email: string;
  role: string;
}