import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { loyaltypointRoutingModule } from './loyaltypoint-routing.module';
import { loyaltypointComponent } from './loyaltypoint.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field'
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
    imports: [CommonModule, FormsModule, loyaltypointRoutingModule, 
    MatTableModule,
      MatPaginatorModule,
      MatFormFieldModule,
      MatInputModule, PageHeaderModule, ReactiveFormsModule],
    declarations: [loyaltypointComponent]
})
export class loyaltypointModule {}
