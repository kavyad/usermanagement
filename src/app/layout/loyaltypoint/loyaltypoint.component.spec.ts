import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { loyaltypointComponent } from './loyaltypoint.component';

describe('loyaltypointComponent', () => {
    let component: loyaltypointComponent;
    let fixture: ComponentFixture<loyaltypointComponent>;

    beforeEach(
        async(() => {
            TestBed.configureTestingModule({
                declarations: [loyaltypointComponent]
            }).compileComponents();
        })
    );

    beforeEach(() => {
        fixture = TestBed.createComponent(loyaltypointComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
