import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { loyaltypointComponent } from './loyaltypoint.component';

const routes: Routes = [
    {
        path: '',
        component: loyaltypointComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class loyaltypointRoutingModule {}
