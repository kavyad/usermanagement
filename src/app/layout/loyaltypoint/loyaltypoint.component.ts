import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { routerTransition } from '../../router.animations';
import * as XLSX from 'xlsx';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import 'jspdf-autotable';

import { loyaltypointService } from '../../../providers/loyaltypoint.service';
import { loyaltyService } from '../../../providers/loyalty.service';

@Component({
    selector: 'app-loyaltypoint',
    templateUrl: './loyaltypoint.component.html',
    styleUrls: ['./loyaltypoint.component.scss'],
      animations: [routerTransition()]
})
export class loyaltypointComponent implements OnInit {
    loyaltypointform = {
      'loyalty': '',
      'pointsredeemed': ''    
    };
    public loyaltypointdata: any;
    user;
    userName;
    loyaltyPoints;
    points = [];
    expirydate;
    expirydateArray = [];
    showTable = true;
    isAdmin = true;

    displayedColumns = ['name', 'points', 'username', 'totalpoints', 'redeemedon', 'expiredate'];
    dataSource: MatTableDataSource<loyaltydisplay>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    exportData = [];

    constructor(private LoyaltyService: loyaltyService, private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public LoyaltypointService: loyaltypointService) {
      this.user = JSON.parse(localStorage.getItem('currentUser'));
      if(this.user['role_id'] != "1" && this.user['role_id'] != "2"){
        this.isAdmin = false;
      }
      this.LoyaltyService.getLoyalty().then(data =>{
        console.log(data);
        this.loyaltyPoints = data['loyalty'];
        for (let i = 0; i < data['loyalty'].length; i++) {
          this.points[data['loyalty'][i]['id']] = data['loyalty'][i]['points'];
          this.expirydateArray[data['loyalty'][i]['id']] = data['loyalty'][i]['expiryDate'];
        }
      })
    }
    
    ngOnInit() {
      if(!this.isAdmin){
        this.userName = this.user['firstName']+' '+this.user['lastName'];
        this.LoyaltypointService.getloyaltypoints(this.user['admin_listid']).then(data => {
            console.log(data['loyalPoints']);
            const loyalty: loyaltydisplay[] = [];
            for (let i = 0; i < data['loyalPoints'].length; i++) { loyalty.push(data['loyalPoints'][i]); this.exportData.push(data['loyalPoints'][i]);}
            this.dataSource = new MatTableDataSource(loyalty);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            //this.users = data['user'];
        },
        error => {
        });
      }
      else{
        this.LoyaltypointService.getallloyaltypoints().then(data => {
            console.log(data['loyalPoints']);
            const loyalty: loyaltydisplay[] = [];
            for (let i = 0; i < data['loyalPoints'].length; i++) { loyalty.push(data['loyalPoints'][i]); this.exportData.push(data['loyalPoints'][i]);}
            this.dataSource = new MatTableDataSource(loyalty);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            //this.users = data['user'];
        },
        error => {
        });
      }
    }

    redeemPoints(){
      this.loyaltypointdata = {
        loyalty: this.loyaltypointform.loyalty,
        userid: this.user['id'],
        pointsredeemed: this.loyaltypointform.pointsredeemed,
        remainingpoints: (parseInt(this.user['points']) - parseInt(this.loyaltypointform.pointsredeemed)),
        expirydate: this.expirydate
      }
      this.LoyaltypointService.addloyaltypoint(this.loyaltypointdata).then(data => {
        console.log(data);
        if(data['status'] == 'success'){
          window.location.reload();
        }
      },
      error => {
      });
    }

    toggleDisplay() {
      this.showTable = false;
    }

    setPoints(value){
      this.loyaltypointform.pointsredeemed = this.points[value];
      this.expirydate = this.expirydateArray[value];
    }
    
    ExportToPdf(){
      console.log(this.exportData);
      var doc = new jspdf();
      var col = ['Name', 'Points', 'User Name', 'Total Points', 'Redeemed On', 'Expire Date'];
      var rows = [];
      this.exportData.forEach(element => {      
        var temp = [element.loyalty, element.points_redeemed, element.user_id, element.remaining_points, element.redeemed_on, element.expiry_date];
        rows.push(temp);
      }); 
      doc.autoTable(col, rows, { startY: 10 });    
      doc.save('renewal_'+ new Date().getTime() +'.pdf');
    }

    ExportTOExcel(){
        const workBook = XLSX.utils.book_new();
        const workSheet = XLSX.utils.json_to_sheet(this.exportData);

        XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
        XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.xlsx');
    }

    ExportTOCsv(){
        const workBook = XLSX.utils.book_new();
        const workSheet = XLSX.utils.json_to_sheet(this.exportData);
        const csv = XLSX.utils.sheet_to_csv(workSheet);

        XLSX.utils.book_append_sheet(workBook, workSheet, 'data');
        XLSX.writeFile(workBook, 'renewal_'+ new Date().getTime() +'.csv');
    }
}
export interface loyaltydisplay {
  name: string;
  points: string;
  username: string;
  totalpoints: string;
  redeemedon: string;
  expiredate: string;
}