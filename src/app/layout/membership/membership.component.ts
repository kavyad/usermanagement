import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { MembershipService } from '../../../providers/membership.service';
import { ViewChild } from '@angular/core';

@Component({
    selector: 'app-membership',
    templateUrl: './membership.component.html',
    styleUrls: ['./membership.component.scss'],
    animations: [routerTransition()]
})
export class MembershipComponent implements OnInit {
  [x: string]: any;
	displayedColumns = ['membershipname', 'price','duration'];
	dataSource: MatTableDataSource<membershipData>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	public membership: any;
  membershipForm: FormGroup;
	public memDetail: any;
	public memberships: any;
	showMembership: boolean;
    constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public membershipservice: MembershipService, public router: Router) {}

    ngOnInit() {
      this.showMembership = false;
    	this.membershipForm = this.formBuilder.group({
    		memname: ['', Validators.required],
			description: ['', Validators.required],
			duration: ['', Validators.required],
			price: ['', Validators.required]
        });
		this.membershipservice.getMembership().then(data => {
	        console.log(data['membership']);

          if(data['success']){
  	        const membership: membershipData[] = [];
  	        for (let i = 0; i < data['membership'].length; i++) {membership .push(data['membership'][i]); }
  	        this.dataSource = new MatTableDataSource(membership);
      			this.dataSource.paginator = this.paginator;
      			this.dataSource.sort = this.sort;
          }
	        //this.users = data['user'];
	    },
	    error => {
	    });
	}

    	applyFilter(filterValue: string) {
    		filterValue = filterValue.trim(); // Remove whitespace
    		filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    		this.dataSource.filter = filterValue;
    	}

    	editmembership(id){
    		this.router.navigate(['/edit-membership'],{queryParams:{id:id}});
    	}

  	// deletemembership(id){
  	// 	this.membershipservice.deletemembership(id).then(data => {
  	//         window.location.reload();
  	//     },
  	//     error => {
  	//     });
  	// }
    get f() { return this.membershipForm.controls; }
  	memToggle(){
      	this.showMembership = true;
  	}
    cancel(){
      window.location.reload();
    }
	addMembership(){
		this.memDetail = {
			memname: this.f.memname.value,
			description: this.f.description.value,
			duration: this.f.duration.value,
			price: this.f.price.value
		}
		this.membershipservice.addMembership(this.memDetail).then(data => {
			if(data['status']){
    			this.showMembership = false;
				this.router.navigate(['/membership']);
			}
		});
	}
}
  export interface membershipData {
    membershipname: string;
    price: boolean;
    duration: string;
  }
