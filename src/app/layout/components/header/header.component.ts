import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { SettingsService } from '../../../../providers/settings.service'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    pushRightClass: string = 'push-right';
    public users: any;
    public settingDetail = {
        'webname': '',
        'sitedir': '',
        'companylogo': '',
        'webemail': '',
        'payemail': '',
        'invoicelogo': '',
        'datetype': '',
        'allowregistration': '',
        'regverification': '',
        'registrationnotification': '',
        'defaultcurrency': '',
        'webphone': '',
        'listadmin': ''
    };
    isAdmin = true;
    isListAdmin = false;

    constructor(private translate: TranslateService, public router: Router, public settingsservice: SettingsService) {

        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });

        let users = JSON.parse(localStorage.getItem('currentUser'));
        if(users['role_id'] == "4"){  // list admin role id
            this.isListAdmin = true;
            localStorage.setItem('isListAdmin', 'true');
        }
        else if (users['role_id'] != "1" && users['role_id'] != "2"){ //admin / super admin role id
          this.isAdmin = false;          
        }
    }

    ngOnInit() {
        this.users = JSON.parse(localStorage.getItem('currentUser'));
        this.settingsservice.getSettings().then(data => {
            if(data['status']){
                this.settingDetail.webname = data['settings'][0].webname;
                this.settingDetail.sitedir = data['settings'][0].sitedir;
                this.settingDetail.companylogo = data['settings'][0].companylogo;
                this.settingDetail.webemail = data['settings'][0].webemail;
                this.settingDetail.payemail = data['settings'][0].payemail;
                this.settingDetail.invoicelogo = data['settings'][0].invoicelogo;
                this.settingDetail.datetype = data['settings'][0].datetype;
                this.settingDetail.allowregistration = data['settings'][0].allowregistration;
                this.settingDetail.regverification = data['settings'][0].regverification;
                this.settingDetail.registrationnotification = data['settings'][0].registrationnotification;
                this.settingDetail.defaultcurrency = data['settings'][0].defaultcurrency;
                this.settingDetail.webphone = data['settings'][0].webphone;
                this.settingDetail.listadmin = data['settings'][0].listadmin;
                localStorage.setItem('listadmin', this.settingDetail.listadmin);
            }
        });
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('currentUser');
        localStorage.removeItem('listadmin');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
}
