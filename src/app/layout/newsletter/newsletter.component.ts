import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { EmailTemplateService } from '../../../providers/email-template.service';
import { newsLetterService } from '../../../providers/newsletter.service';
import { ListsService } from '../../../providers/lists.service';

import { QuillModule } from 'ngx-quill';
import Quill from 'quill';
import ImageResize from 'quill-image-resize-module';
Quill.register('modules/imageResize', ImageResize);

@Component({
    selector: 'app-newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.scss'],
    animations: [routerTransition()]
})
export class NewsletterComponent implements OnInit {
	public newsLetter= {
		'name': '',
		'subject': '',
		'list':'Select List',
		'to': '',
		'cc': '',
		'bcc': '',
		'body': ''
	};
	newsletterDetails: any;
	showTable = true;
	listDetails: any;
	condition = false;
	editor_modules = {};

	@ViewChild('editor') editor: QuillModule

    constructor(private listsservice: ListsService, private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public newsletterservice: newsLetterService, public emailtemplateservice: EmailTemplateService) {
    	this.editor_modules = {
			toolbar: {
			container: [
			  [{ 'font': [] }],
			  [{ 'size': ['small', false, 'large', 'huge'] }],
			  ['bold', 'italic', 'underline', 'strike'],
			  [{ 'header': 1 }, { 'header': 2 }],
			  [{ 'color': [] }, { 'background': [] }],
			  [{ 'list': 'ordered' }, { 'list': 'bullet' }],
			  [{ 'align': [] }],
			  ['link', 'image']
			]
			},
			imageResize: true
		};
    }

    ngOnInit() {
		this.newsletterservice.getNewletter().then(data => {
			if(data['status']){
				this.newsletterDetails = data['newsletter'];
			}
		});
    	this.listsservice.getlist().then(data => {
	        console.log(data['lists']);
	        this.listDetails = data['lists'];
	    },
	    error => {
	    });
	}
	addnews(){
		this.showTable = false;
	}
	cancel(){
		window.location.reload();
	}
	saveNewsletter(){
		this.newsletterservice.saveNewletter(this.newsLetter).then(data => {
			if(data['status']){
				window.location.reload();
			}
		});
	}
	sendnewsletter(id){
		this.newsletterservice.sendNewletter(id).then(data => {
			if(data['status']){
				window.location.reload();
			}
		});
	}
	checkvalue(val){
		console.log(val);
		if(val != "Select List"){
			this.condition = true;
		}
		else{
			this.condition = false;
		}
	}
	setFocus(event){
		console.log(event);
	}
	sendtestmail(){
		console.log(this.newsLetter);
	}
}
