import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { NewsletterRoutingModule } from './newsletter-routing.module';
import { NewsletterComponent } from './newsletter.component';
import { PageHeaderModule } from './../../shared';
import { ReactiveFormsModule }    from '@angular/forms';
import { QuillModule } from 'ngx-quill';
import { AngularFileUploaderModule } from "angular-file-uploader";

@NgModule({
    imports: [CommonModule, FormsModule, NewsletterRoutingModule, PageHeaderModule, ReactiveFormsModule, QuillModule, AngularFileUploaderModule],
    declarations: [NewsletterComponent]
})
export class NewsletterModule {}
