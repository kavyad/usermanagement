import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ListsRoutingModule } from './lists-routing.module';
import { ListsComponent } from './lists.component';
import { PageHeaderModule } from './../../shared';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field'
import { ReactiveFormsModule }    from '@angular/forms';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
    imports: [CommonModule,
              ListsRoutingModule,
              FormsModule,
              PageHeaderModule,
              MatTableModule,
              MatPaginatorModule,
              MatFormFieldModule,
              MatInputModule, 
              ReactiveFormsModule,
              GooglePlaceModule],
    declarations: [ListsComponent]
})
export class ListsModule {}
