import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

import { ListsService } from '../../../providers/lists.service';

@Component({
    selector: 'app-lists',
    templateUrl: './lists.component.html',
    styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {
  [x: string]: any;
	displayedColumns = ['listname', 'email','phonenumber','address','action'];
	dataSource: MatTableDataSource<listsData>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

	@ViewChild("placesRef") placesRef : GooglePlaceDirective;

	public listDetails: any;
	lists = {
		'id': '',
		'name': '',
		'email': '',
		'phonenum': '',
		'address': '',
		'image': '',
		'newsletter': ''
	};
	public showTable = true;
	public save: any;
    constructor(private listsservice: ListsService) {}

    ngOnInit() {
    	this.listsservice.getlist().then(data => {
	        console.log(data['lists']);
          if(data['status'] == 'success'){
            const lists: listsData[] = [];
            for (let i = 0; i < data['lists'].length; i++) {lists .push(data['lists'][i]); }
            this.dataSource = new MatTableDataSource(lists);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          }
	        this.listDetails = data['lists'];
	    },
	    error => {
	    });
    }

	public handleAddressChange(address) {
        // Do some stuff.
        this.lists.address = address.formatted_address;
        //alert(this.lists.address);
    }

    addlist(){
		this.showTable = false;
		this.save = true;
	}

	cancel(){
		this.showTable = true;
		this.save = true;
	}

	editlist(id){
        this.listsservice.getlistById(id).then(data => {
	        console.log(data['lists']);
	        this.lists.id = data['lists'][0].id;
	        this.lists.name = data['lists'][0].name;
	        this.lists.email = data['lists'][0].email;
	        this.lists.phonenum = data['lists'][0].phone;
	        this.lists.address = data['lists'][0].address;
	        this.lists.image = data['lists'][0].image;
	        this.lists.newsletter = data['lists'][0].newsletter;

			this.save = false;
			this.showTable = false;
	    },
	    error => {
	    });
	}

	deletelist(id){
        this.listsservice.deletelistById(id).then(data => {
	        console.log(data['lists']);
	        window.location.reload();
	    },
	    error => {
	    });
	}

	list(){
		if(this.lists.id != ''){
			this.listsservice.addlist(this.lists, 'update').then(data => {
				if(data['status']){
					window.location.reload();
				}
			});
		}
		else{
			this.listsservice.addlist(this.lists, 'add').then(data => {
				if(data['status']){
					window.location.reload();
				}
			});
		}
	}
}
export interface listsData {
  listname: string;
  email: string;
  phonenumber: string;
  address: string;
  action: string;
}
