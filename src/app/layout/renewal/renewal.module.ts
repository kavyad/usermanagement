import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RenewalRoutingModule } from './renewal-routing.module';
import { RenewalComponent } from './renewal.component';
import { MatTabsModule, MatTableModule, MatPaginatorModule, MatInputModule } from '@angular/material';

@NgModule({
    imports: [CommonModule, RenewalRoutingModule, MatTabsModule, MatTableModule, MatPaginatorModule, MatInputModule],
    declarations: [RenewalComponent]
})
export class RenewalModule {}
