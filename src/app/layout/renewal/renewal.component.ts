import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { RenewalService } from '../../../providers/renewal.service';

@Component({
    selector: 'app-renewal',
    templateUrl: './renewal.component.html',
    styleUrls: ['./renewal.component.scss']
})
export class RenewalComponent implements OnInit {
	displayedColumns = ['transactionId', 'username', 'MemPlan', 'expiryDate'];
	dataSource: MatTableDataSource<RenewalData>;

	dataSourcePending: MatTableDataSource<RenewalData>;

	@ViewChild(MatPaginator) paginator: MatPaginator;
	@ViewChild(MatSort) sort: MatSort;

    constructor(public renewalservice: RenewalService) {}

    ngOnInit() {
    	this.renewalservice.getRenewal().then(data => {
    		if(data['status'] == 'success'){
    			const renewals: RenewalData[] = [];
    			const pendingRenewals: RenewalData[] = [];
	    		for (let i = 0; i < data['user'].length; i++) {
	    			if(data['user'][i]['renewal'] == '0'){
	    				renewals.push(data['user'][i]); 
	    			}
	    			else{
	    				pendingRenewals.push(data['user'][i]); 
	    			}
	    		}
		        this.dataSource = new MatTableDataSource(renewals);
				this.dataSource.paginator = this.paginator;
				this.dataSource.sort = this.sort;

		        this.dataSourcePending = new MatTableDataSource(pendingRenewals);
				this.dataSourcePending.paginator = this.paginator;
				this.dataSourcePending.sort = this.sort;
			}
    	});
    }
}

export interface RenewalData {
  transactionId: string;
  username: string;
  MemPlan: string;
  expiryDate: string;
}