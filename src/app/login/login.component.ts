import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { routerTransition } from '../router.animations';
import { AuthenticationService } from '../../providers/authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public authenticationService: AuthenticationService) {}
    loginForm: FormGroup;
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    get f() { return this.loginForm.controls; }
    onLoggedin() {
        if (this.loginForm.invalid) {
            alert('Enter Username/Password.');
            return;
        }
    	this.authenticationService.login(this.f.username.value, this.f.password.value).then(data => {
            console.log(data);
            if (data && data['status'] == 'success') {
        		localStorage.setItem('isLoggedin', 'true');
                this.router.navigate(['/dashboard']);
            }
            else{
                alert('Invalid Credentials Please try again.');
            }
        },
        error => {
        });
    }
}
