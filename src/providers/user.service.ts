import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getUsers() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsers.php?q='+this.timestamp)
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUser(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+this.timestamp,{ id })
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    addUser(userDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addUser.php?q='+this.timestamp, { userDetail: userDetail })
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    editUser(userDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/editUser.php?q='+this.timestamp, { userDetail })
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    deleteUser(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteUser.php?q='+this.timestamp, { id })
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersSubscribtion(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+this.timestamp+'&action=all')
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersSubscriptionList(listId){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+this.timestamp+'&action=list&listId='+listId)
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getGroups(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getGroups.php?q='+this.timestamp)
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
}