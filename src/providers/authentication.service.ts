import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    login(username, password) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/login.php?q='+this.timestamp, { username: username, password: password })
            .subscribe(user => {
                console.log(user);
                // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`
                if (user && user['status'] == 'success') {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user['user']));
                }

                resolve(user);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
    
    addregister(registerDetail) {
          return new Promise(resolve => {
          this.http.post('https://click365.com.au/usermanagement/register.php', { registerDetail })
              .subscribe(user => {
                  console.log(user);
                  // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`
                  // if (user && user['status'] == 'success') {
                  //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                  //     localStorage.setItem('currentUser', JSON.stringify(user['user']));
                  // }

                  resolve(user);
                  }, err => {
                 console.log("vbn"+err);
              });
          });
      }

    uploadFile(file){    
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type':  'application/json'
        //     })
        // };    
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/uploadfile.php?q='+this.timestamp, file)
            .subscribe(user => {
                console.log(user);
                // login successful if there's a jwt token in the response `${config.apiUrl}/users/authenticate`, httpOptions
                resolve(user);
            }, err => {
               console.log("vbn - "+JSON.stringify(err));
            });
        });
    }
}