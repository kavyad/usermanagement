import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class MembershipService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getMembership() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getMembership.php?q='+this.timestamp)
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
    addMembership(memDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addMembership.php?q='+this.timestamp, { memDetail: memDetail })
            .subscribe(users => {
                console.log(users);               
                resolve(users);
                }, err => {
               console.log("vbn"+err);
            });
        });
    }
}