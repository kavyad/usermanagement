import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class RenewalService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getRenewal() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/renewal.php?q='+this.timestamp)
            .subscribe(renewal => {
                console.log(renewal);               
                resolve(renewal);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}