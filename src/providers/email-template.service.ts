import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class EmailTemplateService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();
    defaultTemplate = '<table border="0" cellspacing="0" cellpadding="0" style="width: 600px; padding: 10px;"><tbody><tr><td height="20">&nbsp;</td></tr><tr><td align="center">Place Logo</td></tr><tr><td height="10">&nbsp;</td></tr><tr style="font-size: 20px;text-align: center;font-weight: bold;"><td><br><p>Welcome to User Membership</p><br /></td></tr><tr><td><table border="0" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td>Dear [Name],</td></tr><tr></tr><tr><td height="5">&nbsp;</td></tr><tr></tr><tr><td>Type Details........</td></tr></tbody></table></td></tr><tr><td height="25">&nbsp;</td></tr><tr><td><table border="0" width="100%" cellspacing="0" cellpadding="0" align="center" style="background-color: #000;"><tbody><tr><td align="left"><p style="color: #ffffff;text-align: center; font-weight: normal; line-height: 20px;margin-top: 10px;">©[DATE] User Management</p></td><td width="30">&nbsp;</td></tr><tr></tr><tr><td height="25">&nbsp;</td></tr></tbody></table></td></tr></tbody></table>';

    getDefaultTemplate() {
        return this.defaultTemplate;
    }
    getEmailTemp() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getEmailTemplates.php?q='+this.timestamp)
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addEmailTemp(emTemDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addEmailTemplate.php?q='+this.timestamp, { emTemDetail })
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getEmailTemplateById.php?q='+this.timestamp, { id })
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    updateEmailTemp(emTemupdateDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/updateEmailTemplate.php?q='+this.timestamp, { emTemupdateDetail })
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteEmailTempById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteEmailTemplateById.php?q='+this.timestamp, { id })
            .subscribe(emailtem => {
                console.log(emailtem);               
                resolve(emailtem);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}